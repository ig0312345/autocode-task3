## Task description ##

> In a small town *initialPopulation* is the population at the beginning of a year.
> The population regularly increases by some *percents* per year 
> and moreover *visitors*   (new inhabitants per year) come to live in the town.
> How many years does the town need to see its population greater or equal to *currentPopulation* inhabitants

To DO:
Fork this project to your gitLab
Implement your code
Build solution
Push your changes
Submit your solution using AutoCode application
See your Build and tests results
Note, that you may Submit your solution not more than 3 times for every task
Good luck and Happy coding!



